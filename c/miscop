/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* Title:   miscop.c
 * Purpose: miscellanaous operations on a ColourDbox Object
 * Author:  TGR
 * History: 4-Mar-94: TGR: created
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kernel.h"
#include "swis.h"

#include "tboxlibint/const.h"
#include "tboxlibint/macros.h"
#include "tboxlibint/debug.h"
#include "tboxlibint/mem.h"
#include "tboxlibint/messages.h"
#include "tboxlibint/string32.h"

#include "tboxlibint/objects/toolbox.h"
#include "tboxlibint/objects/colourdbox.h"

#include "auxiliary.h"
#include "object.h"
#include "task.h"

#include "miscop.h"

_kernel_oserror *colourdbox_get_window_id       (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_get_dialogue_handle (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_set_colour          (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_get_colour          (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_set_colour_model    (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_get_colour_model    (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_set_none_available  (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_get_none_available  (_kernel_swi_regs *r, TaskDescriptor *t);

/* removed owing to bug in ColourPicker
_kernel_oserror *colourdbox_set_help_message    (_kernel_swi_regs *r, TaskDescriptor *t);
_kernel_oserror *colourdbox_get_help_message    (_kernel_swi_regs *r, TaskDescriptor *t);
*/

#define MAX_MISCOP_METHODS 8

static _kernel_oserror *(*colourdbox_miscop_methods [MAX_MISCOP_METHODS]) (_kernel_swi_regs *r, TaskDescriptor *t) = {
   colourdbox_get_window_id,
   colourdbox_get_dialogue_handle,
   colourdbox_set_colour,
   colourdbox_get_colour,
   colourdbox_set_colour_model,
   colourdbox_get_colour_model,
   colourdbox_set_none_available,
   colourdbox_get_none_available
/* removed ...
   colourdbox_set_help_message,
   colourdbox_get_help_message
*/
};

extern _kernel_oserror *miscop_object (_kernel_swi_regs *r, TaskDescriptor *t)
{

    /*
     * do a "miscellaneous (ie object-specific) operation on an object
     * R0 = 6
     * R1 = Object ID
     * R2 = internal handle returned when Object was created
     * R3 = wimp task handle of caller (use to identify task descriptor)
     * R4 -> user regs R0-R9
     *      R0 =  flags
     *      R1 =  Object ID
     *      R2 =  method code
     *      R3-R9 method-specific data
     */

    /*
     * This is the routine which deals with all object-specific operations.
     *
     *
     */

  _kernel_swi_regs   *user_regs = (_kernel_swi_regs *) r->r[4];
   int                 method    = user_regs->r[2];

   DEBUG debug_output ("y","ColourDbox: miscop method = 0x%x\n",method);

   if (method < 0 || method >= MAX_MISCOP_METHODS) {
      return make_error_hex(ColourDbox_NoSuchMiscOpMethod,1,method);
   } else
      return (*colourdbox_miscop_methods[method])(r,t);

   IGNORE(t);

   return NULL;
}

_kernel_oserror *colourdbox_get_window_id       (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_swi_regs     *user_regs        = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal   *internal         = (ColourDboxInternal *) r->r[2];

   DEBUG debug_output ("y","ColourDbox: WIMP handle is 0x%x\n",internal->window_handle);

   user_regs->r[0] = (int) internal->window_handle;

   IGNORE(t);

   return NULL;
}

_kernel_oserror *colourdbox_get_dialogue_handle (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_swi_regs     *user_regs        = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal   *internal         = (ColourDboxInternal *) r->r[2];

   DEBUG debug_output ("y","ColourDbox: dialogue handle is 0x%x\n",internal->dialogue_handle);

   user_regs->r[0] = (int) internal->dialogue_handle;

   IGNORE(t);

   return NULL;
}


/* The following routine sets the default colour and, if displayed, the current colour **********/

_kernel_oserror *colourdbox_set_colour          (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_oserror           *e;
   _kernel_swi_regs           regs,
                             *user_regs            = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal        *internal             = (ColourDboxInternal *) r->r[2];
   ColourDescriptorBlock     *colour_block         = (ColourDescriptorBlock *) user_regs->r[3];
   int                        extension_size       = 0;
   ColourPickerExtended      *colourpicker_extd;
   BOOL                       select_none          = user_regs->r[0] & 1,
                              clear_none           = user_regs->r[0] & 2;

   if (colour_block) extension_size = colour_block->hdr.extension_size;

   if ((colourpicker_extd = mem_alloc(sizeof (ColourPickerHeader)+extension_size)) == NULL)
      return make_error (ColourDbox_AllocFailed, 0);

   DEBUG debug_output ("m","ColD: colour block passed in, extension size 0x%x, colour_block @ 0x%x\n",extension_size, colour_block);

   internal->reset_on_close          = 0;
   if (select_none) {
      internal->flags               |=  ColourDboxInternal_SelectNoneButton;
      colourpicker_extd->hdr.flags  |=  ColourPickerFlags_SelectNoneButton;
   } else if (clear_none) {
      internal->flags               &= ~ColourDboxInternal_SelectNoneButton;
      colourpicker_extd->hdr.flags  &= ~ColourPickerFlags_SelectNoneButton;
   }

   if (colour_block) {
      DEBUG debug_dump (colour_block, MAX (8, MIN(extension_size+8, 1024)));
      store_colour_descriptor_block (internal, colour_block, 1);
   }

   if (internal->flags & ColourDboxInternal_IsShowing) {

      DEBUG debug_output ("m","ColD: changing ColourPicker's ColourDescriptor @ 0x%x\n", colourpicker_extd->hdr.descriptor_hdr);

      DEBUG debug_output ("m","ColD: sanity test, %d\n",((int) &(colourpicker_extd->hdr.descriptor_hdr) - (int) colourpicker_extd));

      if (colour_block && internal->colour_block_extd) {
         memcpy (&(colourpicker_extd->hdr.descriptor_hdr), internal->colour_block_extd, sizeof(ColourDescriptorHeader)+extension_size);
      }
      DEBUG debug_dump (colourpicker_extd, 256);

      regs.r[0] = ((select_none|clear_none) ? ColourPicker_Update_SelectNoneButton : 0)
                | ((extension_size)         ? ColourPicker_Update_ModelAndSetting  :
                  ((colour_block)           ? ColourPicker_Update_RGBSetting       : 0));

      regs.r[1] = internal->dialogue_handle;
      regs.r[2] = (int) colourpicker_extd;

      if ((e = _kernel_swi (ColourPicker_UpdateDialogue, &regs, &regs)) != NULL) {
         if (e->errnum == 0x20d02) {
            e = dialogue_hidden (internal);
            goto clearup1;
         } else {
            goto clearup1;
         }
      }
   }
   IGNORE(t);

   mem_freek (colourpicker_extd);

   return NULL;
   clearup1:
      mem_freek (colourpicker_extd);
      return e;
}

_kernel_oserror *colourdbox_get_colour          (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_oserror           *e;
   _kernel_swi_regs           regs,
                             *user_regs            = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal        *internal             = (ColourDboxInternal *) r->r[2];
   ColourDescriptorHeader    *colour_block         = (ColourDescriptorHeader *) user_regs->r[3];
   ColourPickerHeader        *colourpicker_block;
   ColourPickerExtended      *colourpicker_extd;
   int                        size                 = user_regs->r[4];
   int                        reqdsize;

   DEBUG debug_output ("m","ColD: getting colour\n");

   if (internal->flags & ColourDboxInternal_IsShowing) {

      regs.r[0] = 0;
      regs.r[1] = internal->dialogue_handle;
      regs.r[2] = 0;

      DEBUG debug_output ("m","ColD: first call to ColourPicker_ReadDialogue\n");

      if ((e = _kernel_swi (ColourPicker_ReadDialogue, &regs, &regs)) != NULL) {
         if (e->errnum == 0x20d02) {
            e = dialogue_hidden (internal);
            goto not_showing;
         } else {
            return e;
         }
      }
      user_regs->r[4] = regs.r[2] + sizeof (ColourDescriptorHeader) - sizeof (ColourPickerHeader);

      /*     +------------+---------------+
       *     |            | flags, title, |   To find out what the top-right bit of
       *     | Colour     | coordinates,  |  this diagram's length is, take the size
       *     |            | reserved      |  of ColourPickerHeader and subtract the
       *     |            |       fields. |  Size of ColourDescriptorHeader
       *     |            |               |
       *     |   Picker   +---------------+
       *     |            |               |
       *     |            | Colour        |
       *     |            |   Descriptor  |
       *     |     Header |        Header |
       *     |            |               |
       *     +------------+---------------+
       */

      DEBUG debug_output ("m","ColD: sizes: asked for 0x%x, full block 0x%x, picker header 0x%x, descriptor 0x%x\n",regs.r[2]);

      if (size && (size < (regs.r[2] + sizeof (ColourDescriptorHeader) - sizeof (ColourPickerHeader))))
         return make_error (ColourDbox_ShortBuffer, 0);

      if ((colourpicker_block = mem_alloc (regs.r[2])) == NULL)
         return make_error (ColourDbox_AllocFailed, 0);

      regs.r[1] = internal->dialogue_handle;
      regs.r[2] = (int) colourpicker_block;

      DEBUG debug_output ("y","ColD: second call to ColourPicker\n");

      if ((e = _kernel_swi (ColourPicker_ReadDialogue, &regs, &regs)) != NULL)
         goto clearup1;

      DEBUG debug_output ("y","ColourDbox: flags = 0x%x\n",colourpicker_block->flags);

      user_regs->r[0] = (colourpicker_block->flags & ColourPickerFlags_SelectNoneButton) ? 1 : 0;

      if (!size || !colour_block) {
         DEBUG debug_output ("m","ColD: no size given, returning\n");
         mem_freek (colourpicker_block);
         return NULL;
      }
      DEBUG debug_dump (colourpicker_block, sizeof colourpicker_block);

      colourpicker_extd = (ColourPickerExtended *) colourpicker_block;

      memcpy (colour_block, &colourpicker_extd->hdr.descriptor_hdr, colourpicker_block->descriptor_hdr.extension_size+sizeof(ColourDescriptorHeader));

      DEBUG debug_dump (colour_block, colourpicker_block->descriptor_hdr.extension_size+sizeof(ColourDescriptorHeader));

      mem_freek (colourpicker_block);
   } else { not_showing:
      DEBUG debug_output ("y","ColD: not showing\n");
      user_regs->r[0] = (internal->flags & ColourDboxInternal_SelectNoneButton) ? 1 : 0;
      reqdsize = internal->colour_block_extd->hdr.extension_size + sizeof(ColourDescriptorHeader);
      if (!size || !colour_block) {
         DEBUG debug_output ("y","ColDbox: request for size");
         user_regs->r[4] = reqdsize;
      } else {
         if (size >= reqdsize) {
            memcpy (colour_block, internal->colour_block_extd, reqdsize);
         } else {
            return make_error (ColourDbox_ShortBuffer, 0);
         }
      }
   }
   DEBUG debug_output ("m","ColD: ended GetColour\n");

   IGNORE(t);

   return NULL;

   clearup1:
      if (colourpicker_block) mem_freek (colourpicker_block);
      return e;
}

_kernel_oserror *colourdbox_set_colour_model    (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_oserror           *e;
   _kernel_swi_regs           regs,
                             *user_regs            = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal        *internal             = (ColourDboxInternal *) r->r[2];
   ColourModelBlock          *colourmodel_blk      = (ColourModelBlock *) user_regs->r[3];
   int                        colour_descriptor_block_size;
   size_t                     colour_picker_extd_size;

   /* check that we can deal with this colour model */

   if (colourmodel_blk == NULL || colourmodel_blk->ext.model >= 3) return NULL;

   e = store_colour_model_block (internal, colourmodel_blk, 1);

   if (e != NULL)
      return e;

   colour_descriptor_block_size = sizeof (ColourDescriptorHeader) + colourmodel_blk->extension_size;
   colour_picker_extd_size = sizeof(ColourPickerHeader) + colourmodel_blk->extension_size;

   if (internal->flags & ColourDboxInternal_IsShowing) {
      ColourPickerExtended *colourpicker_extd;

      if ((colourpicker_extd = mem_alloc (colour_picker_extd_size)) == NULL)
         return make_error (ColourDbox_AllocFailed, 0);

      memcpy (&colourpicker_extd->hdr.descriptor_hdr, internal->colour_block_extd, colour_descriptor_block_size);

      regs.r[0] = ColourPicker_Update_ModelAndSetting;
      regs.r[1] = internal->dialogue_handle;
      regs.r[2] = (int) colourpicker_extd;

      if ((e = _kernel_swi (ColourPicker_UpdateDialogue, &regs, &regs)) != NULL) {
         if (e->errnum == 0x20d02) {
            e = dialogue_hidden (internal);
         }
         mem_freek (colourpicker_extd);
         return e;
      }

      mem_freek (colourpicker_extd);
   }
   IGNORE(t);

   return NULL;
}

_kernel_oserror *colourdbox_get_colour_model    (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_oserror           *e;
   _kernel_swi_regs           regs,
                             *user_regs            = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal        *internal             = (ColourDboxInternal *) r->r[2];
   ColourModelBlock          *colourmodel_blk      = (ColourModelBlock *) user_regs->r[3];
   ColourPickerExtended      *colourpicker_extd;
   int                        size                 = user_regs->r[4];
   size_t                     reqdsize;


   if (internal->flags & ColourDboxInternal_IsShowing) {

      regs.r[0] = 0;
      regs.r[1] = internal->dialogue_handle;
      regs.r[2] = 0;

      if ((e = _kernel_swi (ColourPicker_ReadDialogue, &regs, &regs)) != NULL) {
         if (e->errnum == 0x20d02) {
            dialogue_hidden (internal);
            goto not_showing;
         } else {
            return e;
         }
      }
      reqdsize = regs.r[2] + sizeof (int) - sizeof (ColourPickerHeader);
      user_regs->r[4] = reqdsize;

      if (size && (size < reqdsize))
         return make_error (ColourDbox_ShortBuffer, 0);

      if ((colourpicker_extd = mem_alloc (regs.r[2])) == NULL)
         return make_error (ColourDbox_AllocFailed, 0);

      regs.r[1] = internal->dialogue_handle;
      regs.r[2] = (int) colourpicker_extd;

      if ((e = _kernel_swi (ColourPicker_ReadDialogue, &regs, &regs)) != NULL)
         goto clearup1;

      if (!size || !colourmodel_blk) {
         DEBUG debug_output ("m","ColD: no size given, returning\n");
         mem_freek (colourpicker_extd);
         return NULL;
      }
      memcpy (colourmodel_blk, &colourpicker_extd->hdr.descriptor_hdr.extension_size, reqdsize);

      mem_freek (colourpicker_extd);
   } else { not_showing:
      reqdsize = sizeof (int) + internal->colour_block_extd->hdr.extension_size;

      if (!colourmodel_blk)
      {
         user_regs->r[4] = reqdsize;
         return NULL;
      }
      if (size < reqdsize)
         return make_error (ColourDbox_ShortBuffer, 0);
      if (internal->colour_block_extd) {
         memcpy (colourmodel_blk, &internal->colour_block_extd->hdr.extension_size, reqdsize);
         user_regs->r[4] = reqdsize;
      } else {
         user_regs->r[4] = 0;
      }
   }
   IGNORE(t);

   return NULL;

   clearup1:
      mem_freek (colourpicker_extd);
      return e;
}

_kernel_oserror *colourdbox_set_none_available  (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_oserror           *e;
   _kernel_swi_regs          *user_regs            = (_kernel_swi_regs *) r->r[4],
                              regs;
   ColourDboxInternal        *internal             = (ColourDboxInternal *) r->r[2];
   BOOL                       none_available       = user_regs->r[3];
   int                        colourpicker_flags;

   DEBUG debug_output ("m","ColourDbox: none %savaliable\n",none_available?"":"not ");

   if (none_available) {
      internal->flags |=  ColourDboxInternal_IncludeNoneButton;
   } else {
      internal->flags &= ~ColourDboxInternal_IncludeNoneButton;
   }
   if (~internal->flags & ColourDboxInternal_IsShowing)
      return NULL;

   colourpicker_flags = (none_available) ? ColourPickerFlags_IncludeNoneButton : 0;

   regs.r[0]          = ColourPicker_Update_IncludeNoneButton;
   regs.r[1]          = internal->dialogue_handle;
   regs.r[2]          = (int) &colourpicker_flags;

   if ((e = _kernel_swi (ColourPicker_UpdateDialogue, &regs, &regs)) != NULL) {
      if (e->errnum == 0x20d02) {
         e = dialogue_hidden (internal);
      } else {
         return e;
      }
   }
   IGNORE(t);


   return e;
}

_kernel_oserror *colourdbox_get_none_available  (_kernel_swi_regs *r, TaskDescriptor *t) {

   _kernel_swi_regs          *user_regs            = (_kernel_swi_regs *) r->r[4];
   ColourDboxInternal        *internal             = (ColourDboxInternal *) r->r[2];

   DEBUG debug_output ("m","ColourDbox: none %savaliable\n",(internal->flags & ColourDboxInternal_IncludeNoneButton)?"":"not ");

   user_regs->r[0] = (internal->flags & ColourDboxInternal_IncludeNoneButton) ? 1: 0;

   IGNORE(t);

   return NULL;
}

/* removed owing to bug in ColourPicker

 *_kernel_oserror *colourdbox_set_help_message    (_kernel_swi_regs *r, TaskDescriptor *t) {

 *   _kernel_swi_regs        *user_regs   = ((_kernel_swi_regs *) r->r[4]);
 *   ColourDboxInternal      *internal    = (ColourDboxInternal *) r->r[2];
 *   char                    *src_txt     = (char *) user_regs->r[3];

 *   if (!string_copy_chk (internal->help_message,src_txt,internal->max_help))
 *      return make_error (ColourDbox_ShortBuffer,0);

 *   IGNORE(t);

 *   return NULL;
 *}

 *_kernel_oserror *colourdbox_get_help_message    (_kernel_swi_regs *r, TaskDescriptor *t) {

 *   _kernel_swi_regs        *user_regs   = ((_kernel_swi_regs *) r->r[4]);
 *   ColourDboxInternal      *internal    = (ColourDboxInternal *) r->r[2];
 *   char                    *dest_txt    = (char *) user_regs->r[3];
 *   char                    *src_txt     = (char *) internal->help_message;
 *   int                      str_len     = strlen(src_txt);

 *   if (src_txt) {
 *      if (dest_txt) {
 *         if (str_len < user_regs->r[4])
 *            strcpy(dest_txt, src_txt);
 *         else
 *            return make_error(ColourDbox_ShortBuffer,0);
 *      }
 *   } else {
 *      if (user_regs->r[4]) {
 *         strcpy (dest_txt, "\0");
 *         str_len = 0;
 *      } else
 *         return make_error(ColourDbox_ShortBuffer,0);
 *   }
 *   user_regs->r[4] = str_len+1;

 *   IGNORE(t);

 *   return NULL;
 *}

 **/

